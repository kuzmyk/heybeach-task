package com.heybeach.heybeach.entity

data class Beach(val id: String, val url: String)