package com.heybeach.heybeach.network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.NetworkInfo
import android.widget.ImageView
import java.lang.ref.WeakReference

class BitmapDownloadTask(networkInfo: NetworkInfo, imageView: ImageView) :
    DownloadTask<Bitmap>(networkInfo, null, { stream -> BitmapFactory.decodeStream(stream) }) {

    private val imageViewReference = WeakReference<ImageView>(imageView)

    override fun onPostExecute(result: Resource<Bitmap>) {
        imageViewReference.get()?.setImageBitmap(result.data)
    }
}
