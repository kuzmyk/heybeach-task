package com.heybeach.heybeach.network

interface DownloadCallback<T> {

    /**
     * Indicates that the callback handler needs to update its appearance or information based on
     * the result of the task. Expected to be called from the main thread.
     */
    fun downloadFinished(result: Resource<T>)
}