package com.heybeach.heybeach.network

import android.net.NetworkInfo
import android.os.AsyncTask
import java.io.IOException
import java.io.InputStream
import java.net.URL

/**
 * Implementation of AsyncTask designed to fetch data from the network.
 */
open class DownloadTask<T>(
    private val networkInfo: NetworkInfo,
    private val callback: DownloadCallback<T>?,
    private val streamReader: (stream: InputStream) -> T
) : AsyncTask<String, Int, Resource<T>>() {

    /**
     * Cancel background network operation if we do not have network connectivity.
     */
    override fun onPreExecute() {
        if (!networkInfo.isConnected) {
            // If no connectivity, cancel task and update Callback with null data.
            callback?.downloadFinished(Resource.error("NO NETWORK"))
            cancel(true)
        }
    }

    /**
     * Defines work to perform on the background thread.
     */
    override fun doInBackground(vararg urls: String): Resource<T> {
        var result: Resource<T>? = null

        if (!isCancelled && urls.isNotEmpty()) {
            val urlString = urls[0]
            result = try {
                val url = URL(urlString)
                val response = downloadUrl(url, streamReader)
                if (response != null) {
                    Resource.success(response)
                } else {
                    throw IOException("No response received.")
                }
            } catch (e: Exception) {
                Resource.error(e.toString())
            }
        }

        return result ?: Resource.error("NULL")
    }

    /**
     * Updates the DownloadCallback with the result.
     */
    override fun onPostExecute(result: Resource<T>) {
        callback?.downloadFinished(result)
    }

    /**
     * Override to add special behavior for cancelled AsyncTask.
     */
    override fun onCancelled(result: Resource<T>) {}

    /**
     * Wrapper class that serves as a union of a result value and an exception. When the download
     * task has completed, either the result value or exception can be a non-null value.
     * This allows you to pass exceptions to the UI thread that were thrown during doInBackground().
     */
}
