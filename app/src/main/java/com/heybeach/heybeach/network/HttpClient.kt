package com.heybeach.heybeach.network

import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

/**
 * Given a URL, sets up a connection and gets the HTTP response body from the server.
 * If the network request is successful, it converts input stream to type T. Otherwise,
 * it will throw an IOException.
 */
@Throws(IOException::class)
fun <T> downloadUrl(url: URL, reader: (stream: InputStream) -> T): T? {
    var connection: HttpURLConnection? = null
    return try {
        connection = (url.openConnection() as? HttpURLConnection)
        connection?.run {
            // Timeout for reading InputStream arbitrarily set to 3000ms.
            readTimeout = 3000
            // Timeout for connection.connect() arbitrarily set to 3000ms.
            connectTimeout = 3000
            // For this use case, set HTTP method to GET.
            requestMethod = "GET"
            // Already true by default but setting just in case; needs to be true since this request
            // is carrying an input (response) body.
            doInput = true
            // Open communications link (network traffic occurs here).
            connect()
            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw IOException("HTTP error code: $responseCode")
            }
            // Retrieve the response body as an InputStream.
            inputStream?.let { stream ->
                // Converts Stream to String with max length of 500.
                reader.invoke(stream)
            }
        }
    } finally {
        // Close Stream and disconnect HTTPS connection.
        connection?.inputStream?.close()
        connection?.disconnect()
    }
}
