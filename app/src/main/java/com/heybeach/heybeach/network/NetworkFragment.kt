package com.heybeach.heybeach.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

private const val TAG = "NetworkFragment"

class NetworkFragment : Fragment(), NetworkLoader {
    private lateinit var networkInfo: NetworkInfo

    private var mCallback: DownloadCallback<String>? = null

    private var dataDownloadTask: StringDownloadTask? = null
    private var imageDownloadTask: BitmapDownloadTask? = null

    companion object {

        fun getInstance(fragmentManager: FragmentManager): NetworkFragment {
            // Recover NetworkFragment in case we are re-creating the Activity due to a config change.
            // This is necessary because NetworkFragment might have a task that began running before
            // the config change occurred and has not finished yet.
            // The NetworkFragment is recoverable because it calls setRetainInstance(true).
            var networkFragment = fragmentManager.findFragmentByTag(TAG) as? NetworkFragment
            if (networkFragment == null) {
                networkFragment = NetworkFragment()
                fragmentManager.beginTransaction()
                        .add(networkFragment, TAG)
                        .commit()
            }
            return networkFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        networkInfo = (context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        retainInstance = true
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // Host Activity will handle callbacks from task.
        mCallback = context as? DownloadCallback<String>
    }

    override fun onDetach() {
        super.onDetach()
        // Clear reference to host Activity to avoid memory leak.
        mCallback = null
    }

    override fun onDestroy() {
        // Cancel task when Fragment is destroyed.
        cancelDownload()
        super.onDestroy()
    }

    override fun cancelDownload() {
        dataDownloadTask?.cancel(true)
        imageDownloadTask?.cancel(true)
    }

    override fun downloadString(url: String, callback: DownloadCallback<String>) {
        dataDownloadTask = StringDownloadTask(networkInfo, callback)
        dataDownloadTask?.execute(url)
    }

    override fun downloadImage(url: String, image: ImageView) {
        imageDownloadTask = BitmapDownloadTask(networkInfo, image)
        imageDownloadTask?.execute(url)
    }
}
