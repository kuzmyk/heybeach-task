package com.heybeach.heybeach.network

import android.widget.ImageView

interface NetworkLoader {
    fun downloadString(url: String, callback: DownloadCallback<String>)

    fun downloadImage(url: String, image: ImageView)

    fun cancelDownload()
}