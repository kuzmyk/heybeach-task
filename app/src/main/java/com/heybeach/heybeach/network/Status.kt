package com.heybeach.heybeach.network

enum class Status {
    SUCCESS, ERROR, LOADING
}
