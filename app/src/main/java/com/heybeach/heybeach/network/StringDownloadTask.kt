package com.heybeach.heybeach.network

import android.net.NetworkInfo
import java.io.*

class StringDownloadTask(networkInfo: NetworkInfo, callback: DownloadCallback<String>) :
    DownloadTask<String>(networkInfo, callback, { stream -> readStreamToString(stream) })

/**
 * Converts the contents of an InputStream to a String.
 */
@Throws(IOException::class, UnsupportedEncodingException::class)
private fun readStreamToString(stream: InputStream): String {
    val maxReadSize = 5000
    val reader = InputStreamReader(stream, "UTF-8")
    val rawBuffer = CharArray(maxReadSize)
    val buffer = StringBuffer()
    var readSize: Int = reader.read(rawBuffer)
    var maxReadBytes = maxReadSize
    while (readSize != -1 && maxReadBytes > 0) {
        if (readSize > maxReadBytes) {
            readSize = maxReadBytes
        }
        buffer.append(rawBuffer, 0, readSize)
        maxReadBytes -= readSize
        readSize = reader.read(rawBuffer)
    }
    return buffer.toString()
}
