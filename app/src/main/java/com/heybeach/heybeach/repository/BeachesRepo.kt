package com.heybeach.heybeach.repository

import com.heybeach.heybeach.entity.Beach
import com.heybeach.heybeach.network.DownloadCallback
import com.heybeach.heybeach.network.NetworkLoader
import com.heybeach.heybeach.network.Resource

const val BASE_URL = "http://techtest.lab1886.io:3000/"
private const val BEACHES_URL = BASE_URL + "beaches"

class BeachesRepo(private val loader: NetworkLoader) {
    fun getBeaches(callback: (Resource<List<Beach>>) -> Unit) {
        try {
            loader.downloadString(BEACHES_URL, object : DownloadCallback<String> {
                override fun downloadFinished(result: Resource<String>) {
                    if (result.data != null) {
                        val beaches = parseBeaches(result.data)
                        callback.invoke(Resource.success(beaches))
                    } else {
                        callback.invoke(Resource.error(result.message!!))
                    }
                }
            })
        } catch (e: Exception) {
            callback.invoke(Resource.error(e.toString()))
        }
    }
}
