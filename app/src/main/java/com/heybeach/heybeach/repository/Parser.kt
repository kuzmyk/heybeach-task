package com.heybeach.heybeach.repository

import com.heybeach.heybeach.entity.Beach
import org.json.JSONArray
import org.json.JSONObject

fun parseBeaches(json: String): List<Beach> {
    val array = JSONArray(json)
    val list = mutableListOf<Beach>()

    for (i in 0 until array.length()) {
        val beachJson = array[i] as JSONObject
        list.add(Beach(beachJson.getString("_id"), BASE_URL + beachJson.getString("url")))
    }

    return list
}
