package com.heybeach.heybeach.ui

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import com.heybeach.heybeach.R


class AutoGridLayoutManager(context: Context) : GridLayoutManager(context, numberOfColumns(context))

fun numberOfColumns(context: Context): Int {
    val width = context.resources.displayMetrics.widthPixels
    val minColumnWidth = context.resources.getDimensionPixelSize(R.dimen.min_column_width)
    return width / minColumnWidth
}
