package com.heybeach.heybeach.ui.gallery

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.heybeach.heybeach.R
import com.heybeach.heybeach.network.NetworkFragment
import com.heybeach.heybeach.network.NetworkLoader
import com.heybeach.heybeach.repository.BeachesRepo
import com.heybeach.heybeach.ui.AutoGridLayoutManager
import kotlinx.android.synthetic.main.activity_gallery.*

class GalleryActivity : AppCompatActivity() {
    private lateinit var networkLoader: NetworkLoader
    private lateinit var repo: BeachesRepo
    private lateinit var galleryAdapter: GalleryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)

        networkLoader = NetworkFragment.getInstance(supportFragmentManager)
        galleryAdapter = GalleryAdapter(networkLoader)
        repo = BeachesRepo(networkLoader)

        gallery_recycler_view.layoutManager = AutoGridLayoutManager(this)
        gallery_recycler_view.adapter = galleryAdapter
    }

    override fun onStart() {
        super.onStart()
        repo.getBeaches { res -> galleryAdapter.submitList(res.data) }
    }
}
