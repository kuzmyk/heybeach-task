package com.heybeach.heybeach.ui.gallery

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.heybeach.heybeach.R
import com.heybeach.heybeach.entity.Beach
import com.heybeach.heybeach.network.NetworkLoader

class GalleryAdapter(private val loader: NetworkLoader) : ListAdapter<Beach, GalleryViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_gallery, parent, false)
        return GalleryViewHolder(view, loader)
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private val diffCallback = object : DiffUtil.ItemCallback<Beach>() {
    override fun areItemsTheSame(oldItem: Beach, newItem: Beach): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Beach, newItem: Beach): Boolean {
        return oldItem == newItem
    }
}

class GalleryViewHolder(itemView: View, private val loader: NetworkLoader) : RecyclerView.ViewHolder(itemView) {
    private val imageView: ImageView = itemView.findViewById(R.id.item_gallery_image)

    fun bind(beach: Beach) {
        loader.downloadImage(beach.url, imageView)
    }
}
